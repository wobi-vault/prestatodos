'use strict';
angular
  .module('app', [])
  .directive('limitTo', [
    function() {
      return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
          var limit = parseInt(attrs.limitTo);
          angular.element(elem).on('keypress', function(e) {
            var key;
            if (e.which == null) {
              // IE
              key = e.keyCode;
            }
            if (e.which != 0) {
              // all but IE
              key = e.which;
            }
            if (
              this.value.length == limit &&
              (key != 8 && key !== 46 && key !== undefined)
            ) {
              e.preventDefault();
            }
          });
        }
      };
    }
  ])
  .controller('home', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('Prestatodos home');
      sessionStorage.clear();

      $scope.setOrigin = function(origin) {
        sessionStorage.setItem('origin', origin);
      };
    }
  ])
  .controller('step1', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('Prestatodos step1');

      $scope.onlyNumbers = /^\d+$/;
      $scope.emailExpression = /^\w+([\.\+-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
      $scope.loading = false;

      $scope.f = {
        email: '',
        phone: '',
        origin: sessionStorage.getItem('origin')
      };

      $scope.register = function(isValid) {
        // Formulario válido
        if (isValid) {
          $scope.loading = true;
          $http.post('/lead', $scope.f).then(
            res => {
              fbq('track', 'Lead');
              sessionStorage.setItem('leadId', res.data.id);
              window.location = '../results';
            },
            err => {
              console.error(err);
            }
          );
        }
      };
    }
  ])
  .controller('step2', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('Prestatodos step2');

      if (!sessionStorage.getItem('leadId')) {
        window.location = '../step1';
      }

      $scope.loading = true;
      $scope.f = {
        income: '',
        checkIncome: '',
        lead: sessionStorage.getItem('leadId')
      };

      $http.get('/income?sort=id').then(
        res => {
          $scope.income = res.data;
          $scope.loading = false;
        },
        err => {
          console.error(err);
        }
      );

      $scope.register = function(isValid) {
        console.log('register');
        // Formulario válido
        if (isValid) {
          $scope.loading = true;
          if (sessionStorage.getItem('applicationId')) {
            $http
              .patch(
                '/application/' + sessionStorage.getItem('applicationId'),
                $scope.f
              )
              .then(
                res => {
                  sessionStorage.setItem('incomeId', $scope.f.income);
                  sessionStorage.setItem('checkIncome', $scope.f.checkIncome);
                  window.location = '../step3';
                },
                err => {
                  console.error(err);
                }
              );
          } else {
            $http.post('/application', $scope.f).then(
              res => {
                sessionStorage.setItem('incomeId', $scope.f.income);
                sessionStorage.setItem('applicationId', res.data.id);
                sessionStorage.setItem('checkIncome', $scope.f.checkIncome);
                window.location = '../step3';
              },
              err => {
                console.error(err);
              }
            );
          }
        }
      };
    }
  ])
  .controller('step3', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('Prestatodos step3');

      if (!sessionStorage.getItem('leadId')) {
        window.location = '../step1';
      } else if (!sessionStorage.getItem('applicationId')) {
        window.location = '../step2';
      }

      $scope.loading = true;
      $scope.f = {
        profession: ''
      };

      $http.get('/profession?sort=id').then(
        res => {
          $scope.profession = res.data;
          $scope.loading = false;
        },
        err => {
          // Sweetalert
          console.error(err);
        }
      );

      $scope.register = function(isValid) {
        // Formulario válido
        if (isValid) {
          $scope.loading = true;
          $http
            .patch(
              '/application/' + sessionStorage.getItem('applicationId'),
              $scope.f
            )
            .then(
              res => {
                sessionStorage.setItem('professionId', $scope.f.profession);
                window.location = '../step4';
              },
              err => {
                console.error(err);
              }
            );
        }
      };
    }
  ])
  .controller('step4', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('Prestatodos step4');

      if (!sessionStorage.getItem('leadId')) {
        window.location = '../step1';
      } else if (!sessionStorage.getItem('applicationId')) {
        window.location = '../step2';
      }

      $scope.f = {
        debit: ''
      };

      $scope.register = function(isValid) {
        // Formulario válido
        if (isValid) {
          $scope.loading = true;
          $http
            .patch(
              '/application/' + sessionStorage.getItem('applicationId'),
              $scope.f
            )
            .then(
              res => {
                sessionStorage.setItem('debit', $scope.f.debit);
                window.location = '../step5';
              },
              err => {
                console.error(err);
              }
            );
        }
      };
    }
  ])
  .controller('step5', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('Prestatodos step5');

      if (!sessionStorage.getItem('leadId')) {
        window.location = '../step1';
      } else if (!sessionStorage.getItem('applicationId')) {
        window.location = '../step2';
      }

      $scope.loading = true;
      $scope.f = {
        paymentUpToDate: null,
        financialProduct: []
      };

      $http.get('/financialProduct?sort=id').then(
        res => {
          $scope.financialProduct = res.data;
          $scope.loading = false;
        },
        err => {
          // Sweetalert
          console.error(err);
        }
      );

      $scope.change = function(id) {
        var index = $scope.f.financialProduct.indexOf(id);
        if (index >= 0) {
          $scope.f.financialProduct.splice(index, 1);
        } else {
          $scope.f.financialProduct.push(id);
        }
        $scope.f.paymentUpToDate =
          $scope.f.financialProduct.length > 0 ?
          $scope.f.paymentUpToDate :
          null;
      };

      $scope.register = function(isValid) {
        // Formulario válido
        if (isValid) {
          $scope.loading = true;
          $http
            .patch(
              '/application/' + sessionStorage.getItem('applicationId'),
              $scope.f
            )
            .then(
              res => {
                window.location = '../step6';
              },
              err => {
                console.error(err);
              }
            );
        }
      };
    }
  ])
  .controller('step6', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('Prestatodos step6');

      $scope.loading = true;
      if (!sessionStorage.getItem('leadId')) {
        window.location = '../step1';
      } else if (!sessionStorage.getItem('applicationId')) {
        window.location = '../step2';
      }

      var date = new Date();
      date.setFullYear(date.getFullYear() - 18);
      date.setMonth(0);
      date.setDate(1);

      $scope.months = [
        'ENE',
        'FEB',
        'MAR',
        'ABR',
        'MAY',
        'JUN',
        'JUL',
        'AGO',
        'SEP',
        'OCT',
        'NOV',
        'DIC'
      ];

      $scope.f = {
        dayBirth: 1,
        monthBirth: 'ENE',
        yearBirth: date.getFullYear(),
        state: null,
        termsConditions: null,
        privacyNotice: null
      };

      $scope.daysInMonth = function() {
        var month = $scope.months.indexOf($scope.f.monthBirth) + 1;
        $scope.days = $scope.range(
          1,
          new Date($scope.f.yearBirth, month, 0).getDate(),
          1
        );
        console.log($scope.f);
      };

      $scope.range = function(min, max, step) {
        step = Math.abs(step) || 1;
        var input = [];
        var range = Math.abs(max - min) + 1;
        for (var i = 0; i < range; i++) {
          input.push(min);
          min = max - min >= 0 ? min + step : min - step;
        }
        return input;
      };

      $scope.years = $scope.range(
        date.getFullYear(),
        date.getFullYear() - 82,
        1
      );
      $scope.daysInMonth();

      $http.get('/state?sort=description').then(
        res => {
          $scope.state = res.data;
          $scope.f.state = $scope.state[0].id;
          $scope.loading = false;
        },
        err => {
          // Sweetalert
          console.error(err);
        }
      );

      $scope.register = function(isValid) {
        // Formulario válido
        if (isValid) {
          $scope.loading = true;
          $http
            .patch('/lead/' + sessionStorage.getItem('leadId'), $scope.f)
            .then(
              res => {
                $http
                  .patch(
                    '/application/' + sessionStorage.getItem('applicationId'),
                    $scope.f
                  )
                  .then(
                    res => {
                      sessionStorage.setItem('stateId', $scope.f.state);
                      sessionStorage.setItem('yearBirth', $scope.f.yearBirth);
                      // fbq('track', 'Lead');
                      window.location = '../results';
                    },
                    err => {
                      console.error(err);
                    }
                  );
              },
              err => {
                console.error(err);
              }
            );
        }
      };
    }
  ])
  .controller('email', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('Prestatodos email');

      $scope.lettersForNames = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ-\s]+$/;
      $scope.onlyNumbers = /^\d+$/;
      $scope.emailExpression = /^\w+([\.\+-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

      $scope.f = {};

      $scope.change = function(prod) {
        $scope.f.products = $scope.f.products ? $scope.f.products : [];
        var index = $scope.f.products.indexOf(prod);
        if (index >= 0) {
          $scope.f.products.splice(index, 1);
        } else {
          $scope.f.products.push(prod);
        }
        console.log($scope.f.products);
      };

      $scope.sendEmail = function(isValid) {
        // Formulario válido
        if (isValid && !$scope.sending) {
          $scope.sending = true;
          $http.post('/email', $scope.f).then(
            res => {
              $scope.sending = false;
              window.location.reload();
            },
            err => {
              $scope.sending = false;
              console.error(err);
            }
          );
        }
      };
    }
  ])
  .controller('results', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      $scope.loading = true;
      console.log('Prestatodos results');

      $http.get('/card?active=true&sort=order').then(
        res => {
          $scope.cards = res.data;
          // var incomeLess7k = false;
          // var showCreditea = false;

          $scope.cards.forEach((c, i) => {
            c.description = c.description.split(String.fromCharCode(10));
            c.urls = [{
              link: c.url
            }];
            // if (c.id == 1) {
            //   c.urls = [{
            //     name: 'Android',
            //     link: 'https://app.appsflyer.com/mx.com.bancoazteca.bazdigitalmovil?pid=leadgenios_int&c=enrolamientoG&af_channel=SM&af_adset=SM&af_ad=250_SM_221119&af_click_lookback=7d&clickid=#reqid#&is_retargeting=true&utm_source=250&utm_medium=SM&utm_campaign=250_SM_221119'
            //   }, {
            //     name: 'Iphone',
            //     link: 'https://app.appsflyer.com/id1203433478?pid=leadgenios_int&c=enrolamientoG&af_channel=SM&af_adset=SM&af_ad=250_SM_221119&af_click_lookback=7d&clickid=#reqid#&is_retargeting=true&utm_source=250&utm_medium=SM&utm_campaign=250_SM_221119'
            //   }];
            // }
          });

          // var less7kId = ['1', null]; // less 7k
          // var excludedIncomeCT = ['1', '2', null]; // less 15k
          // var excludedStates = ['7', '26', '27', '30']; // Chihuahua, Sonora, Tabasco, Veracruz
          // var crediteaId = 4;
          // var moneymanId = 3;
          //
          // var now = new Date();
          // var profession = sessionStorage.getItem('professionId') == 1;
          // var debit = sessionStorage.getItem('debit') == 'true';
          // var checkIncome = sessionStorage.getItem('checkIncome') == 'true';
          // var state = !excludedStates.includes(
          //   sessionStorage.getItem('stateId')
          // );
          // var income = !excludedIncomeCT.includes(
          //   sessionStorage.getItem('incomeId')
          // );
          // var age = now.getFullYear() - sessionStorage.getItem('yearBirth');
          // var adult = age >= 25 && age <= 65;
          // showCreditea =
          //   profession && debit && state && checkIncome && adult && income;
          //
          // incomeLess7k = less7kId.includes(sessionStorage.getItem('incomeId'));
          //
          // if (incomeLess7k) {
          //   // $scope.cards = $scope.cards.filter(c => c.id != moneymanId);
          // } else {
          //   // $scope.cards = $scope.cards.filter(c => c.id != 1 && c.id != 2); // Remove Banco Azteca and Kueski
          //   $scope.cards = $scope.cards.filter(c => c.id != 1); //Remove Banco Azteca
          // }
          // if (!showCreditea) {
          //   $scope.cards = $scope.cards.filter(c => c.id != crediteaId);
          // }

          $scope.loading = false;
        },
        err => {
          console.error(err);
        }
      );
    }
  ])
  .controller('pages', [
    '$scope',
    '$http',
    '$sce',
    '$location',
    function($scope, $http, $sce, $location) {
      var vm = this;
      var path = $location.absUrl().split('/')[3];
      var idPage = 0;

      switch (path) {
        case 'loan':
          idPage = 1;
          break;
        case 'healers':
          idPage = 2;
          break;
        case 'terms':
          idPage = 3;
          break;
        case 'privacy':
          idPage = 4;
          break;
        case 'faq':
          idPage = 5;
          break;
        case 'about':
          idPage = 6;
          break;
      }

      $http.get('/page/' + idPage).then(
        res => {
          $scope.page = res.data;
          $scope.page.htmlContent = $sce.trustAsHtml($scope.page.htmlContent);
        },
        err => {
          console.error(err);
        }
      );
    }
  ]);