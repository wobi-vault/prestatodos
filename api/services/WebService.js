module.exports = {

  lead: function(lead, ip) {
    if (sails.config.webService.active) {
      let mapObj = _.omit(sails.config.webService, 'url', 'endpoint', 'method');
      mapObj.email = lead.email;
      mapObj.day = ('0' + lead.dayBirth).slice(-2);
      mapObj.month = ('0' + Month.toNumber(lead.monthBirth)).slice(-2);
      mapObj.year = lead.yearBirth;
      mapObj.ip = ip;

      let endpoint = sails.config.webService.endpoint.replace(
        new RegExp('\{\\w+\}', 'gim'),
        function(matched) {
          return mapObj[matched.substring(1, matched.length - 1)] || matched;
        }
      );

      console.log(endpoint);

      SendHttp.request(
        sails.config.webService.url,
        endpoint,
        sails.config.webService.method,
        undefined,
        undefined,
        function(err, data) {
          if (err) {
            console.log(err);
            console.log('\nError en webservice, reintentando...\n\n\n');
            WebService.lead(lead);
          } else
            console.log(data);
          console.log('\nPetición webservice exitosa\n\n');
        }
      );
    }
  }

};
